#ifndef _MAINTEST_
#define _MAINTEST_

void test();
void testList();
void testTree();
void testHeap();
void testSort();
void testGeometry();
void testAlgo();

#endif