#include <stdlib.h>
#include <stdio.h>
#include "tree.h"
#include "heap.h"
#include "sort.h"

void ArrayHeapSort( void** A, int N, int (*preceed)(const void*, const void*), void (*viewHeapData)(const void*), void (*freeHeapData)(void*) )
{
	ArrayHeap *AH = ArrayToArrayHeap(A, N, preceed, viewHeapData, freeHeapData); // Reorganise le tableau A pour satisfaire les proprietes d'un tas
	for (int i = N-1; i >= 0; i--)
	{
		A[i] = ArrayHeapExtractMin(AH); // Extraction de l'element de AH avec la plus grande priorite !!Invertion de l'ordre par rapport au preceed!!
	}
	free(AH); // Liberation de la memoire allouee a AH mais pas a A
}

void CBTHeapSort( void** A, int N, int (*preceed)(const void*, const void*), void (*viewHeapData)(const void*), void (*freeHeapData)(void*) )
{
	CBTHeap *H = newCBTHeap(preceed, viewHeapData, freeHeapData);				
	for (int i = 0; i < N; i++) // Insertion des elements de A dans dans le tas H
	{
		CBTHeapInsert(H, A[i]);
	}
	for (int i = 0; i < N; i++) // Extraction des elements de H avec la plus grande priorite et insertion dans A
	{
		A[i] = CBTHeapExtractMin(H);
	}
	freeCBTHeap(H, 0);
}

void SelectionSort( void** A, int N, int (*preceed)(const void*, const void*) )
{
	for(int i = 0; i < N-1; i++) // Recherche de la position PosMin du min en i et N-1
	{
		void *min = A[i];
		int PosMin = i;
		for(int j = i+1; j < N; j++)
		{
			if( preceed(A[j], min) ) // Si la data à A[j] a une plus grande priorité que min, alors min devient A[j]
			{
				min = A[j];
				PosMin = j;
			}
		}
		// Echange de A[i] et A[PosMin]
		void *aux = A[i];
		A[i] = A[PosMin];
		A[PosMin] = aux;
	}
}