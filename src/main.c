#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "util.h"
#include "list.h"
#include "tree.h"
#include "heap.h"
#include "sort.h"
#include "geometry.h"
#include "algo.h"

int main(int argc,char *args[]) {
/*
    if ( argc == 1 )
    {
        char infilename[100], outfilename[100];
        printf("De quel fichier souhaitez vous faire l'enveloppe convexe ?\n\t");
        scanf("%s",infilename);
        printf("Dans quel fichier voulez vous stocker les points de l'enveloppe convexe ?\n\t");
        scanf("%s",outfilename);

        int choix;
        do
        {
            printf("Quel algorithme voulez vous utiliser ?\n\t[1] SlowConvexHull\n\t[2] ConvexHull avec CBTHeapSort\n\t[3] ConvexHull avec ArrayHeapSort\n\t[4] ConvexHull avec SelectionSort\n\t[5] RapidConvexHull\n Entrez votre choix ici : ");
            scanf("%d",&choix);            
        } 
        while ( choix > 5 || choix < 1 );
        
        switch (choix)
        {
            case 1 :
                SlowConvexHull(infilename,outfilename);
                break;
            case 2 :
                ConvexHull(infilename,outfilename,1);
                break;
            case 3 :
                ConvexHull(infilename,outfilename,2);
                break;
            case 4 :
                ConvexHull(infilename,outfilename,3);
                break;
            case 5 :
                RapidConvexHull(infilename,outfilename);
                break;
        }    
    } 
    else if ( argc == 4 )
    {
        int choix = atoi(args[3]);

        if ( choix > 5 || choix < 1 )
        {
            ShowMessage("Votre choix d'algorithme est invalide, il doit etre contenu entre 1 et 5 :\n\t[1] SlowConvexHull\n\t[2] ConvexHull avec CBTHeapSort\n\t[3] ConvexHull avec ArrayHeapSort\n\t[4] ConvexHull avec SelectionSort\n\t[5] RapidConvexHull\n",0);
        }

        switch (choix)
        {
            case 1 :
                SlowConvexHull(args[1],args[2]);
                break;
            case 2 :
                ConvexHull(args[1],args[2],1);
                break;
            case 3 :
                ConvexHull(args[1],args[2],2);
                break;
            case 4 :
                ConvexHull(args[1],args[2],3);
                break;
            case 5 :
                RapidConvexHull(args[1],args[2]);
                break;
            default :
                break;
        }
    }
    else
    {
        ShowMessage("Vos arguments sont mal saisi, il doit en avoir 3 :\n 1er argument : le fichier .txt contenant les points dont vous voulez calculer l'enveloppe convexe\n 2eme argument : le fichier dans lequel vous sera donné la solution\n 3eme argument :\n\t[1] SlowConvexHull\n\t[2] ConvexHull avec CBTHeapSort\n\t[3] ConvexHull avec ArrayHeapSort\n\t[4] ConvexHull avec SelectionSort\n\t[5] RapidConvexHull\n",0);
    }
*/
    int nf = 100;
    char *TF[nf];
    int k = 0;
    TF[k++] = "data/exp/instance_000050_01";
    TF[k++] = "data/exp/instance_000050_05";
    TF[k++] = "data/exp/instance_000050_08";
    TF[k++] = "data/exp/instance_000100_01";
    TF[k++] = "data/exp/instance_000100_05";
    TF[k++] = "data/exp/instance_000100_08";
    TF[k++] = "data/exp/instance_000200_01";
    TF[k++] = "data/exp/instance_000200_05";
    TF[k++] = "data/exp/instance_000200_08";
    TF[k++] = "data/exp/instance_000300_01";
    TF[k++] = "data/exp/instance_000300_05";
    TF[k++] = "data/exp/instance_000300_08";    
    TF[k++] = "data/exp/instance_000400_01";
    TF[k++] = "data/exp/instance_000400_05";
    TF[k++] = "data/exp/instance_000400_08";
    TF[k++] = "data/exp/instance_000500_01";
    TF[k++] = "data/exp/instance_000500_05";
    TF[k++] = "data/exp/instance_000500_08";
    TF[k++] = "data/exp/instance_000600_01";
    TF[k++] = "data/exp/instance_000600_05";
    TF[k++] = "data/exp/instance_000600_08";

    k++;

    clock_t start, end;
    double cpu_time_used;

	FILE *fd = fopen("data/time", "w");
	if(fd==NULL){
		ShowMessage("erreur ouverture fichier",1);
	}
    for (int i = 0; i < k; i++)
    {
        fprintf(fd,"\nFOR %s\n",TF[i]);
        start = clock();
        SlowConvexHull(TF[i],"data/temp");
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        fprintf(fd,"Time used by SlowConvexHull: \t\t\t%lf sec\n", cpu_time_used);

        start = clock();
        ConvexHull(TF[i],"data/temp",1);
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        fprintf(fd,"Time used by ConvexHull-CBTHeapSort: \t%lf sec\n", cpu_time_used);

        start = clock();
        ConvexHull(TF[i],"data/temp",2);
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        fprintf(fd,"Time used by ConvexHull-ArrayHeapSort: \t%lf sec\n", cpu_time_used);

        start = clock();
        ConvexHull(TF[i],"data/temp",3);
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        fprintf(fd,"Time used by ConvexHull-SelectionSort: \t%lf sec\n", cpu_time_used);

        start = clock();
        RapidConvexHull(TF[i],"data/temp");
        end = clock();
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        fprintf(fd,"Time used by RapidConvexHull: \t\t\t%lf sec\n", cpu_time_used); 
    }





    fclose(fd);
    return EXIT_SUCCESS;
}