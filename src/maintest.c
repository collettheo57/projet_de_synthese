#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "util.h"
#include "list.h"
#include "tree.h"
#include "heap.h"
#include "sort.h"
#include "geometry.h"
#include "algo.h"
#include "maintest.h"

void test(){
    int entre=0;
    do
    {
        printf("Quel test souhaitez vous réaliser ?\n\tlist.c\t\t(1)\n\ttree.c\t\t(2)\n\theap.c\t\t(3)\n\tsort.c\t\t(4)\n\tgeometry.c\t(5)\n\talgo.c\t\t(6)\n");
        printf("\nVotre choix : ");
		scanf("%d",&entre);
    } while ( entre > 6 || entre < 1 );
    switch (entre)
    {
    case 1:
        testList();
        break;
    case 2:
        testTree();
        break;
    case 3:
		testHeap();    
        break;
	case 4:
		testSort();
		break;
    case 5:
        testGeometry();
        break;
	case 6:
		testAlgo();
		break;
    default:
        printf("ERREUR 404");
        break;
    }
}

void testList(){
	printf("\n\nTEST list.c\n");

	int *ListData1=newInt(1);
	int *ListData2=newInt(2);
	int *ListData3=newInt(3);
	int *ListData4=newInt(4);

	List *L = newList(&viewInt,&freeInt);
	viewList(L);
	freeList(L,1);

	List *L1 = newList(&viewInt,&freeInt);

	listInsertFirst(L1,ListData2);
	listInsertFirst(L1,ListData1);
	listInsertLast(L1,ListData3);
	listInsertLast(L1,ListData4);

	viewList(L1);

	int *ListData5=newInt(5);
	int *ListData6=newInt(6);
	int *ListData7=newInt(7);
	int *ListData8=newInt(8);

	List *L2=newList(&viewInt,&freeInt);
	
	listInsertLast(L2,ListData7);
	listInsertLast(L2,ListData8);
	listInsertFirst(L2,ListData6);
	listInsertFirst(L2,ListData5);

	viewList(L2);

	List *L3;
	L3=listConcatenate(L1,L2);
	viewList(L3);

	void *listData9=listRemoveLast(L3);
	viewInt(listData9);
	viewList(L3);
	L1->freeData(listData9);

	void *listData10=listRemoveFirst(L3);
	viewInt(listData10);
	viewList(L3);
	L1->freeData(listData10);

	void *listData11=listRemoveNode(L3,Successor(Head(L3)));
	viewInt(listData11);
	viewList(L3);

	listInsertAfter(L3,listData11,Predecessor(Tail(L3)));
	viewList(L3);
	printf("\nGetListSize : ");
	printf("\n\tTaille de la liste : %d\n",getListSize(L3));

	while (getListSize(L3) > 0)
	{
		void *data = listRemoveFirst(L3);
		viewList(L3);
		L3->freeData(data);
	}
	
	freeList(L3,1);
}

void testTree(){
	printf("\n\nTEST tree.c\n");
	
	int N = 15;	
	srand(time(NULL));
	printf("Creation d'un arbre avec %d elements :\n",N);
	CBTree *T=newCBTree(&viewInt,&freeInt);
	for (int i = 0; i < N; i++)
	{
		void *data = newInt(rand()%100);
		viewInt(data);
		printf(" ");
		CBTreeInsert(T,data);
	}

	printf("\ngetCBTreeSize :\n\t\t%d",getCBTreeSize(T));

	printf("\nviewCBTree :");
	printf("\n\tpreorder : ");
	viewCBTree(T,0);
	printf("\n\tpostorder : ");
	viewCBTree(T,1);
	printf("\n\tinorder : ");
	viewCBTree(T,2);

	printf("\nTaille de l'arbre :%d",getCBTreeSize(T));

	printf("\nCBTreeGetLast :");
	TNode *node=CBTreeGetLast(T);
	printf("\n\tDernière valeur de l'arbre = ");
	viewInt(getTNodeData(node));

	printf("\nCBTreeRemove :");
	void *TreeData=CBTreeRemove(T);
	printf("\n\tSupression de la dernière valeur de l'arbre : ");
	viewCBTree(T,2);
	printf("\n\tLa valeur supprimé : ");
	viewInt(TreeData);
	freeInt(TreeData);

	printf("\nCBTreeSwapData : \n\téchange de la première et dernière valeur : ");
	viewCBTree(T,0);
	printf(" --> ");
	CBTreeSwapData(CBTreeGetLast(T),Root(T));
	viewCBTree(T,0);
	printf("\n");
	printf("\nTaille de l'arbre :%d",getCBTreeSize(T));

	printf("\nVidage de l'arbre");
	while (!treeIsEmpty(T))
	{
		void *TreeData=CBTreeRemove(T);
		printf("\n\tLa valeur supprimé : ");
		viewInt(TreeData);
		freeInt(TreeData);		
	}
	printf("\n\tTaille de l'arbre :%d",getCBTreeSize(T));
	freeCBTree(T,1);
	printf("\n");
}

void testHeap(){
	printf("\n\n\nTEST heap.c\n\n");

	printf("\tArrayHeap\n\n");
	int N = 15;	
	srand(time(NULL));
	printf("Creation d'un tableau avec %d elements :\n",N);
	void** T=(void*)calloc(N,sizeof(void*));
	for (int i = 0; i < N; i++)
	{
		T[i]=newInt(rand()%100);
		printf("\tIndice %d: ",i);
		viewInt(T[i]);
		printf("\n");
	}

	printf("\nTransformation du tableau en tas :");
	ArrayHeap *AH=ArrayToArrayHeap(T,N,(&intSmallerThan),(&viewInt),(&freeInt));
	viewArrayHeap(AH);

	void* aux=ArrayHeapExtractMin(AH);
	viewArrayHeap(AH);
	free(aux);

	aux=NULL;
	freeArrayHeap(AH,1);

	printf("\tCBTHeap\n");
	int *CBTHeapData0=newInt(15);	
	int *CBTHeapData1=newInt(10);
	int *CBTHeapData2=newInt(5);
	int *CBTHeapData3=newInt(20);
	int *CBTHeapData4=newInt(22);
	int *CBTHeapData5=newInt(2);
	int *CBTHeapData6=newInt(0);
	int *CBTHeapData7=newInt(50);

	CBTHeap *H = newCBTHeap((&intSmallerThan),(&viewInt),(&freeInt));
	CBTHeapInsert(H,CBTHeapData0);
	CBTHeapInsert(H,CBTHeapData1);
	CBTHeapInsert(H,CBTHeapData2);
	CBTHeapInsert(H,CBTHeapData3);
	CBTHeapInsert(H,CBTHeapData4);
	CBTHeapInsert(H,CBTHeapData5);
	CBTHeapInsert(H,CBTHeapData6);
	CBTHeapInsert(H,CBTHeapData7);

	viewCBTHeap(H);

	void *aux1=CBTHeapExtractMin(H);
	printf("CBTHeapExtractMin :\n\t");
	(H->viewHeapData)(aux1);
	viewCBTHeap(H);

	printf("\nVidage de l'arbre");
	while (!treeIsEmpty(H->T))
	{
		void *TreeData=CBTreeRemove(H->T);
		printf("\n\tLa valeur supprimé : ");
		viewInt(TreeData);
		freeInt(TreeData);		
	}
	printf("\n\tTaille de l'arbre :%d",getCBTreeSize(H->T));

	free(aux1);
	aux1=NULL;
	freeCBTHeap(H,1);
}

void testSort(){
	int N = 15;

	void **T1 = (void **)calloc(N,sizeof(void *));
	void **T2 = (void **)calloc(N,sizeof(void *));
	void **T3 = (void **)calloc(N,sizeof(void *));
	
	srand(time(NULL));
	for (int i = 0; i < N; i++)
	{
		T1[i] = T2[i] = T3[i] = newInt(rand()%100);// Les 3 tableaux possèdent les mêmes pointeurs sur les entiers donc les free une seul fois
	}
	
	printf("SelectionSort :\n");//Fonctionne
	SelectionSort(T1,N,&intSmallerThan);
	printf("\n");
	
	printf("CBTHeapSort :\n");//Fonctionne
	CBTHeapSort(T2,N,&intSmallerThan,&viewInt,&freeInt);
	printf("\n");

	printf("ArrayHeapSort :\n");//Fonctionne
	ArrayHeapSort(T3,N,&intGreaterThan,&viewInt,&freeInt);
	printf("\n");

		//Affichage de test// 
	for (int i = 0; i < N; i++)
	{
		viewInt(T1[i]);
		printf(" ");
		viewInt(T2[i]);
		printf(" ");
		viewInt(T3[i]);
		printf("\n");
	}

	for (int i = 0; i < N; i++)
	{
		free(T1[i]);
	}

	free(T1);
	free(T2);
	free(T3);
}

void testGeometry(){
	printf("\n\nTEST geometry.c");

	Point *p1 = newPoint(1,1);
	Point *p2 = newPoint(2,2);
	Point *p3 = newPoint(3,3);

	printf("\np1 :\n");
	viewPoint(p1);
	printf("\np2 :\n");
	viewPoint(p2);
	printf("\np3 :\n");
	viewPoint(p3);

	if(collinear(p1,p3,p2)){
		printf("\np2 est collineaire à la droite p1p3");
	}
	else{
		printf("\np2 n'est pas collineaire à la droite p1p3");
	}

	if(onLeft(p1,p3,p2)){
		printf("\np2 est à la gauche de la droite p1p3");
	}
	else{
		printf("\np2 n'est pas à la gauche de la droite p1p3");
	} 

	if(onRight(p1,p3,p2)){
		printf("\np2 est à droite de la droite p1p3");
	}
	else{
		printf("\np2 n'est pas à la droite de la droite p1p3");
	}
	if(isIncluded(p1,p3,p2)){
		printf("\np2 est inclue dans la droite p1p3");
	}
	else{
		printf("\np2 n'est pas inclue dans la droite p1p3");
	}

	printf("\n");

	DEdge *DE=newDEdge(p1,p3);
	viewDEdge(DE);
	freeDEdge(DE);

	freePoint(p1);
	freePoint(p2);
	freePoint(p3);

	Point *P26 = newPoint(2,6);
	Point *P66 = newPoint(6,6);
	Point *P46 = newPoint(4,6);

	if(isIncluded(P26,P66,P46)){
		printf("\nP46 est inclue dans la droite P26P66");
	}
	else{
		printf("\nP46 n'est pas inclue dans la droite P26P66");
	} 

	if(isIncluded(P26,P46,P66)){
		printf("\nP66 est inclue dans la droite P26P46");
	}
	else{
		printf("\nP66 n'est pas inclue dans la droite P26P46");
	}

	if(collinear(P26,P66,P46)){
		printf("\nP46 est collineaire a la droite P26P66");
	}
	else{
		printf("\nP46 n'est pas co a la droite P26P66");
	} 

	if(collinear(P26,P46,P66)){
		printf("\nP66 est co a la droite P26P46");
	}
	else{
		printf("\nP66 n'est pas co a la droite P26P46");
	}

	freePoint(P26);
	freePoint(P46);
	freePoint(P66);

	printf("\n");
}

void testAlgo(){
	printf("\nTEST algo.c\n");

	SlowConvexHull("data/data1","dataTest/SlowConvexHull-OutData1");
	SlowConvexHull("data/data2","dataTest/SlowConvexHull-OutData2");
	SlowConvexHull("data/data3","dataTest/SlowConvexHull-OutData3");
	SlowConvexHull("data/data4","dataTest/SlowConvexHull-OutData4");

	ConvexHull("data/data1","dataTest/ConvexHull-OutData1-1", 1);
	ConvexHull("data/data1","dataTest/ConvexHull-OutData1-2", 2);
	ConvexHull("data/data1","dataTest/ConvexHull-OutData1-3", 3);
	ConvexHull("data/data2","dataTest/ConvexHull-OutData2-1", 1);
	ConvexHull("data/data2","dataTest/ConvexHull-OutData2-2", 2);
	ConvexHull("data/data2","dataTest/ConvexHull-OutData2-3", 3);
	ConvexHull("data/data3","dataTest/ConvexHull-OutData3-1", 1);
	ConvexHull("data/data3","dataTest/ConvexHull-OutData3-2", 2);
	ConvexHull("data/data3","dataTest/ConvexHull-OutData3-3", 3);
	ConvexHull("data/data4","dataTest/ConvexHull-OutData4-1", 1);
	ConvexHull("data/data4","dataTest/ConvexHull-OutData4-2", 2);
	ConvexHull("data/data4","dataTest/ConvexHull-OutData4-3", 3);

	RapidConvexHull("data/data1","dataTest/RapidConvexHull-OutData1");
	RapidConvexHull("data/data2","dataTest/RapidConvexHull-OutData2");
	RapidConvexHull("data/data3","dataTest/RapidConvexHull-OutData3");
	RapidConvexHull("data/data4","dataTest/RapidConvexHull-OutData4");

}