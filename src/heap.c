#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "tree.h"
#include "heap.h"
#include "util.h"

/**********************************************************************************
 * ARRAY HEAP
 **********************************************************************************/

int getAHMaxSize(const ArrayHeap* AH) 
{
	return AH->MAX;
}

int getAHActualSize(const ArrayHeap* AH) 
{
	return AH->N;
}

void* getAHNodeAt(const ArrayHeap* AH, int pos) 
{
	return AH->A[pos];
}

void decreaseAHActualSize(ArrayHeap* AH) 
{
	AH->N--;
}

void setAHNodeAt(ArrayHeap* AH, int position, void* newData) 
{
	AH->A[position] = newData;
}

/**
 * @brief Corrige la position de l'élément de la position \p pos
 * du tas \p AH en le comparant avec ses fils et en l'échangeant avec
 * le fils de la plus grande priorité si nécessaire.
 *
 * Procédure récursive.
 * 
 * @param[in] AH 
 * @param[in] pos L'indice de la valeur en mouvement vers le bas.
 */
static void updateArrayHeapDownwards(ArrayHeap* AH,int pos)
{
	int nextPos=pos,posGauche=(2*pos)+1,posDroite=(2*pos)+2;

	if(posGauche<getAHActualSize(AH) && (AH->preceed)(getAHNodeAt(AH,posGauche),getAHNodeAt(AH,nextPos))) // On regarde la priorité de l'élément à la position pos avec l'élément à gauche
	{
		nextPos=posGauche;
	}
	if(posDroite<getAHActualSize(AH) && (AH->preceed)(getAHNodeAt(AH,posDroite),getAHNodeAt(AH,nextPos))) // On regarde la priorité de l'element sorti par le dernier if avec l'élément à droite
	{ 
		nextPos=posDroite;
	}

	if(nextPos!=pos) // Si nextPos est différent de pos alors on appelle la récursivité
	{ 
		void* aux=getAHNodeAt(AH,pos);
		setAHNodeAt(AH,pos,getAHNodeAt(AH,nextPos));
		setAHNodeAt(AH,nextPos,aux);
		updateArrayHeapDownwards(AH,nextPos);
	}
}

ArrayHeap* ArrayToArrayHeap(void** A, int N, int (*preceed)(const void*, const void*), void (*viewHeapData)(const void*), void (*freeHeapData)(void*))
{
	ArrayHeap* AH = (ArrayHeap*)calloc(1,sizeof(ArrayHeap)); // Allouement mémoire
	AH->A = A;
	AH->N = N;
	AH->MAX = N;
	AH->preceed = (*preceed);
	AH->viewHeapData = (*viewHeapData);
	AH->freeHeapData = (*freeHeapData);
	for (int i = getAHActualSize(AH)-1; i >= 0; i--)
	{ 
		updateArrayHeapDownwards(AH,i); // On prend chaque valeur du tableau pour le transformer en tas et en réorganisant les éléments
	}
	return AH;
}

void viewArrayHeap(const ArrayHeap* AH) 
{
	printf("\n");
	for(int i=0; i<AH->N; i++)
	{
		printf("\tIndice %d: ",i);
		(AH->viewHeapData)(AH->A[i]); // On visualise chaque data du tas
		printf("\n");
	}
}

void freeArrayHeap(ArrayHeap* AH, int deletenode) 
{
	if(deletenode == 1) // On vérifie que deletenode égale 1 pour savoir quelle méthode utiliser
	{
		for(int i=0;i<AH->N;i++)
		{
			(*AH->freeHeapData)(AH->A[i]); // On libère la mémoire de chaque data du tas
		}
	}
	free(AH->A);
	free(AH); 
}

void* ArrayHeapExtractMin(ArrayHeap* AH) 
{
	assert(getAHActualSize(AH) > 0);
	void *min = AH->A[0]; // On affecte la première valeur du tableau du tas à min
	setAHNodeAt(AH,0,getAHNodeAt(AH,getAHActualSize(AH)-1)); // La dernière valeur du tas est réaffecter à la valeur position 0
	decreaseAHActualSize(AH); 
	updateArrayHeapDownwards(AH,0); // Réorganisation du tableau
	return min;
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

CBTHeap *newCBTHeap(int (*preceed)(const void*, const void*), void (*viewHeapData)(const void*), void (*freeHeapData)(void*))
{
	CBTHeap *H = (CBTHeap*)calloc(1, sizeof(CBTHeap)); // Allouement mémoire
	H->T = newCBTree(viewHeapData, freeHeapData);
	H->preceed = preceed;
	H->viewHeapData = viewHeapData;
	H->freeHeapData = freeHeapData;
	return H;
}

CBTree* getCBTree( const CBTHeap *H )
{
	return H->T;
}

/**
 * @brief Corrige la position du nœud à la position \p pos
 * de l'arbre raciné à \p node en le comparant avec son père
 * et en l'échangeant avec lui si nécessaire.
 * Le pointeur de fonction \p preceed est utilisé pour la comparaison.
 *
 * Procédure récursive. En descendant, on cherche le premier nœud
 * à corriger qui se trouve dans la position \p pos (de la même façon
 * que dans insertAfterLastTNode). En remontant, on corrige en échangeant
 * avec le père, si besoin.
 * 
 * @param[in] node 
 * @param[in] position 
 * @param[in] preceed 
 */
static void updateCBTHeapUpwards(TNode* node, int position, int (*preceed)(const void*, const void*)) 
{
	assert(node);
	if(position != 0) 
	{
		if(position == 1) // exception de la récursive : poisition == 1, la node à corrigé est le fils gauche de ma node actuel 
		{
			if((preceed(getTNodeData(Left(node)), getTNodeData(node)))) // vérification de la node prioritaire
			{
				CBTreeSwapData(Left(node),node); 
			}
		}
		else if(position == 2) // exception de la récursive : poisition == 2, la node à corrigé est le fils droit de ma node actuel 
		{
			if((preceed(getTNodeData(Right(node)), getTNodeData(node)))) // vérification de la node prioritaire
			{
				CBTreeSwapData(Right(node), node);
			}
		}
		else // Partie récursive 
		{
			int nb = position + 1;
			int h = floor(log2(nb) + 1);
			int k =  nb - ( pow(2, (h - 1)) - 1 );
			int t = pow(2, (h - 1)) / 2;
			if(k <= t) // Si k <= t, la node à la poisition 'position' est dans le sous arbre à gauche de la node actuelle 
			{
				updateCBTHeapUpwards(Left(node), (nb - pow(2, h - 2) - 1), preceed); // appel recursif
				if (preceed(getTNodeData(Left(node)), getTNodeData(node))) // vérification de la node prioritaire entre la node et son fils gauche qui a pu changer 
				{
					CBTreeSwapData(Left(node), node);
				}
				
			}
			else  // Si k > t, la node à la poisition 'position' est dans le sous arbre à droite de la node actuelle 
			{
				updateCBTHeapUpwards(Right(node), (nb - pow(2, h - 1) - 1), preceed); // appel recursif
				if((preceed(getTNodeData(Right(node)), getTNodeData(node)))) // vérification de la node prioritaire entre la node et son fils droit qui a pu changer 
				{
					CBTreeSwapData(Right(node), node);
				}	
			}
		}
	}
}

void CBTHeapInsert(CBTHeap *H, void* data)
{
	CBTreeInsert(getCBTree(H), data); // Insertion de la data dans l'arbre du tas H
	updateCBTHeapUpwards(Root(getCBTree(H)), getCBTreeSize(getCBTree(H))-1, H->preceed); // Réorganisation du tas
}

/**
 * @brief Corrige la position du nœud \p node en le comparant avec ses fils
 * et en l'échangeant avec le fils de la plus grande priorité si nécessaire.
 * Le pointeur de fonction \p preceed est utilisé pour la comparaison.
 *
 * Procédure récursive.
 *
 * NB: Le sous-arbre avec racine \p node ne peut pas être vide.
 * 
 * @param[in] node 
 * @param[in] preceed 
 */
static void updateCBTHeapDownwards(TNode* node, int (*preceed)(const void*, const void*)) 
{
	assert(node);
	TNode *nextNode = node;
	if (Left(node) != NULL) // existence du fils gauche 
	{
		if ((preceed)(getTNodeData(Left(node)), getTNodeData(nextNode))) // comparaison de la node a son fils gauche 
		{
			nextNode = Left(node);
		}
		
		if (Right(node) != NULL) // existence du fils droit
		{
			if ((preceed)(getTNodeData(Right(node)), getTNodeData(nextNode))) // comparaison de nextnode(qui est soit egale a la node ou a son fils gauche) et du fils droit de la node
			{
				nextNode = Right(node);
			}
		}
		if (nextNode != node) // si nextnode != NULL alors un des deux fils à une plus grande priorité donc échange des 2
		{
			CBTreeSwapData(node, nextNode);
			updateCBTHeapDownwards(nextNode , preceed); // appel recursif 
		}
	}
}

void *CBTHeapExtractMin(CBTHeap *H) 
{
	assert(Root(getCBTree(H)));
	if (getCBTreeSize(getCBTree(H))==1) // Exception : le tas n'a qu'une seul valeur donc min correspond à la seule valeur présente dans l'arbre du tas
	{
		return CBTreeRemove(getCBTree(H)); 
	}
	CBTreeSwapData(CBTreeGetLast(getCBTree(H)),Root(getCBTree(H))); // Echange de la derniere valeur de l'arbre du tas avec sa racine
	void *min = CBTreeRemove(getCBTree(H)); // Suppression de la plus petite valeur du tas qui correspond maintenant à la derniere valeur de l'arbre du tas
	updateCBTHeapDownwards(Root(getCBTree(H)),H->preceed); // Réorganisation du tas
	return min;
}

void viewCBTHeap(const CBTHeap *H) 
{
	viewCBTree(getCBTree(H),2); // Parcours Infixé
}

void freeCBTHeap(CBTHeap* H, int deletenode) 
{
	freeCBTree(getCBTree(H),deletenode);
	free(H);
}