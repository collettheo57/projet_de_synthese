#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"
#include "util.h"

/********************************************************************
 * LNode
 ********************************************************************/

LNode * newLNode(void* data)
{
	LNode *E = (LNode *)calloc(1, sizeof(LNode)); // Allouement mémoire
	E->data = data;
	E->pred = NULL;
	E->succ = NULL;
	return E;
}

void* getLNodeData(const LNode* node)
{
	return node->data;
}

LNode* Successor(const LNode* node)
{
	return node->succ;
}

LNode* Predecessor(const LNode* node)
{
	return node->pred;
}

void setLNodeData(LNode* node, void* newData)
{
	node->data = newData;
}

void setSuccessor(LNode* node, LNode* newSucc)
{
	node->succ = newSucc;
}

void setPredecessor(LNode* node, LNode* newPred)
{
	node->pred = newPred;
}

/********************************************************************
 * List
 ********************************************************************/

List *newList(void(*viewData)(const void*), void(*freeData)(void*))
{
	List *L = (List *)calloc(1, sizeof(List)); // Allouement mémoire
	L->freeData = (*freeData); 
	L->viewData = (*viewData);
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
	return L;
}

int listIsEmpty(List *L)
{
	return L->numelm == 0;
}

int getListSize(const List *L)
{
	return L->numelm;
}

LNode *Head(const List *L)
{
	return L->head;
}

LNode *Tail(const List *L)
{
	return L->tail;
}

void increaseListSize(List *L)
{
	L->numelm++;
}

void decreaseListSize(List *L)
{
	L->numelm--;
}

void setListSize(List *L, int newSize)
{
	L->numelm = newSize;
}

void resetListSize(List* L)
{
	L->numelm = 0;
}

void setHead(List *L, LNode *newHead)
{
	L->head = newHead;
}

void setTail(List *L, LNode *newTail)
{
	L->tail = newTail;
}

void freeList(List * L, int deleteData)
{
	if((deleteData != 0) && (deleteData != 1)) // Exception : deleteData doit etre egal à 0 ou 1 pour utiliser freeList
	{
		ShowMessage("list.c : freeList deleteData invalide",1);
	}
	LNode *E = Head(L);
	while(E != NULL) // Parcours de la liste pour supprimer chaque data
	{
		LNode *suc = Successor(E);
		if(deleteData)(L->freeData) (E->data);
		free(E);
		E = suc;
	}
	free(L);
	L = NULL;
}

void viewList(const List *L)
{
	printf("[ ");
	if (listIsEmpty((List *)L)) // Exception : liste vide (cast en List sinon warning) 
	{
		printf("List vide."); 
	}
	else
	{
		for (LNode *E = L->head; E; E = E->succ) // Parcours de la liste pour afficher chaque data
		{
			(L->viewData)(E->data); 
			printf(" ");
		}
	}
    printf("]");
}

void listInsertFirst(List *L, void *data)
{
	LNode *E = newLNode(data);
	if(listIsEmpty(L)) // Exception : liste vide, seul élément tête/queue
	{
		setTail(L, E);
    }
	else // Rattachement à la tête de liste
	{ 
		setSuccessor(E, Head(L)); 
		setPredecessor(Head(L), E);	
	}
	setHead(L, E);
	increaseListSize(L);
}

void listInsertLast(List *L, void *data)
{
	LNode *E = newLNode(data);
	if(listIsEmpty(L)) // Exception : liste vide, seul élément tête/queue
	{
		setHead(L, E);
	} 
	else // Rattachement à la queue de liste
	{
		setSuccessor(Tail(L), E);
		setPredecessor(E, Tail(L));
	}
	setTail(L, E);
	increaseListSize(L);
}

void listInsertAfter(List *L, void *data, LNode *ptrelm)
{
	LNode *E = newLNode(data);
	if(listIsEmpty(L)) // Exception : liste vide, seul élément tête/queue
	{
		setHead(L, E);
		setTail(L, E);
	}
	else if (ptrelm == Tail(L))  // Rattachement à la fin de liste
	{
		setSuccessor(Tail(L), E);
		setPredecessor(E, Tail(L));
		setTail(L, E);
	}
	else // Rattachement de E à la suite de ptrelm dans la liste
	{
		setSuccessor(E, Successor(ptrelm));
		setPredecessor(Successor(E), E);
		setSuccessor(ptrelm, E);
		setPredecessor(E, ptrelm);
	}
	increaseListSize(L);
}

void* listRemoveFirst(List *L)
{
	if(listIsEmpty(L)) // Exception : liste vide
	{
		ShowMessage("list.c listRemoveFirst : La liste est vide",1);
	}
	LNode *E = Head(L);
	setHead(L, Successor(E));
	if(getListSize(L) == 1) // Exception avec une liste de taille 1 car suppression du dernier element
	{
		setTail(L, NULL); 
	}
	else // Si taille > 1 alors tête != null donc modification du predecesseur à null
	{
		setPredecessor(Head(L), NULL);
	}
	void *data = getLNodeData(E);
	free(E);
	E = NULL;
	decreaseListSize(L);
	return data;
}

void* listRemoveLast(List * L)
{
	if(listIsEmpty(L)) // Exception : liste vide
	{
		ShowMessage("list.c listRemoveLast : La liste est vide",1);
	}
	LNode *E = Tail(L);
	setTail(L, Predecessor(E));
	if(getListSize(L) == 1) // Exception avec une liste de taille 1 car supression du dernier element
	{
		setHead(L, NULL); 
	}
	else // Si taille > 1 alors queue != null et modification du successeur à null
	{
		setSuccessor(Tail(L), NULL);
	}
	void *data = getLNodeData(E);
	free(E);
	E = NULL;
	decreaseListSize(L);
	return data;
}

void* listRemoveNode(List *L, LNode *node)
{
	assert(Head(L) && Tail(L));
	if(node == Head(L)) // Exception : la node est egale à la tête de liste donc utilisation listRemoveFirst
	{
		return listRemoveFirst(L);
	}
	else if(node == Tail(L)) // Exception 2 : la node est egale à la queue de liste donc utilisation listRemoveLast
	{
		return listRemoveLast(L);
	}
	else // Detachement de la node à la liste
	{
		setPredecessor(Successor(node), Predecessor(node));
		setSuccessor(Predecessor(node), Successor(node));
		void *data = getLNodeData(node);
		free(node);
		node=NULL;
		decreaseListSize(L);
		return data;
	}
}

List* listConcatenate(List* L1, List* L2) 
{
	if(listIsEmpty(L1)) // Exception : liste 1 vide
	{
		free(L1);
		L1=NULL;
		return L2;
	}
	else // Rattachement de la queue de liste 1 à la tête de liste 2 
	{
		setSuccessor(Tail(L1), Head(L2));
		setPredecessor(Head(L2), Tail(L1));
		setTail(L1, Tail(L2));
		setListSize(L1, getListSize(L1)+getListSize(L2));
		free(L2);
		L2=NULL;
		return L1;
	}
}