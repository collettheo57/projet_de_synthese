#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "algo.h"
#include "geometry.h"
#include "list.h"
#include "tree.h"
#include "util.h"
#include "heap.h"
#include "sort.h"

/**
 * @brief Réalise la lecture d'un ensemble des points à partir du fichier
 * \p filename.
 * Renvoie 2 paramètres : un tableau contenant les points du fichier
 * \p filename, le nombre \p N de ces points.
 *
 * @param[in] filename le nom du fichier qui contient les points d'entrée.
 * @param[out] N le nombre des points dans le fichier \p filename
 * (paramètre de sortie).
 * @return Point** le tableau avec les points du fichier \p filename.
 */
static Point** readInstance(const char* filename, int* N) {
	FILE *fp = fopen(filename,"r");
	if(fp==NULL){
		ShowMessage("Erreur ouverture du fichier",1);
	}
	fscanf(fp,"%d",N);
	Point **TP =(Point **)calloc(*N,sizeof(Point *));
	for (int i = 0; i < *N; i++){
		int x=0,y=0;
		fscanf(fp,"%d %d",&x,&y);
		Point *p=newPoint(x,y);
		TP[i]=p;
	}
	fclose(fp);
	return TP;
}

/**
 * @brief Réalise l'écriture de l'ensemble des points de la liste \p L
 * dans un fichier avec le nom \p filename.
 *
 * @param[in] filename le nom du fichier d'écriture.
 * @param[in] L la liste des points à écrire dans le fichier \p filename.
 */
static void writeSolution(const char* filename, List* L) {
	FILE *fd = fopen(filename, "w");
	if(fd==NULL){
		ShowMessage("erreur ouverture fichier",1);
	}
	LNode *aux = Head(L);
	while(aux!=NULL){
		Point *point = getLNodeData(aux);
		fprintf(fd, "%lli %lli\n", X(point), Y(point));
		aux = Successor(aux);
	}
	fclose(fd);
}

int estDedans(List *L,void *data){
	LNode *look = Head(L);
	while (look != NULL)
	{
		if (getLNodeData(look)==data)
		{
			return 1;
		}
		look=Successor(look);
	}
	return 0;
}

/**
 * @brief Transforme la liste des arcs \p dedges décrivant les arêtes
 * du polygone de l'enveloppe convexe à une liste des points ordonnés
 * dans le sens horaire.
 * 
 * /!\ Detruit la liste dedges /!\
 * 
 * Pour reconstruire l'enveloppe convexe dans le sens horaire l'algorythme cherche l'arc i+1 ayant pour origine la destination de l'arc i
 * Ainsi au cours de l'algorythme les arcs retirés les uns après les autres sont ceux qui se suivent,
 * en récupérant leurs origines avant leurs suppressions, la liste créée est celle des points ordonnés dans le sens horaire.
 * 
 * @param[in] edges la liste des arcs de l'enveloppe convexe
 * @return List* la liste des points de l'enveloppe convexe dans le sens
 * horaire
 */
static List * DedgesToClockwisePoints(List* dedges)
{

	LNode *look = Head(dedges);
	LNode *look2 = Head(dedges);
	List *LP = newList(viewPoint,freePoint);

	while ( !listIsEmpty(dedges) ) // Vide la liste dedges progressivement en supprimant l'arc 'look' une fois son suivant l'arc 'look2' trouvé
	{
		look2 = Head(dedges);
		while ( (getDestination(getLNodeData(look)) != getOrigin(getLNodeData(look2))) && ( look2 != Tail(dedges))  ) //Ce while s'arrete quand 'look2' sera sur l'arc suivant 'look' ou 'look2' == tail(dedges) car pas de recherche de l'arc suivant de la queue etant deja dans LP
		{
			look2 = Successor(look2);
		}
		listInsertLast(LP, getOrigin(getLNodeData(look))); // Insertion dans la liste du point suivant le sens horaire
		void *supp = listRemoveNode(dedges, look);
		dedges->freeData(supp);
		look = look2; // recherche de l'arc suivant celui qui vient d'etre trouvé 
	}
	freeList(dedges,1);
	return LP;
}

void SlowConvexHull(const char* infilename, const char* outfilename)
{
	int N;
	Point **T = readInstance(infilename, &N);
	bool ok = false;
	List *E = newList(viewDEdge, freeDEdge);

	for(int i=0; i < N; i++)// 'i' designe l'indice du point A
	{
		for(int j=0; j < N; j++)// 'j' designe l'indice du point B
		{
			ok = true;
			if(i != j)// un arc (A,A) n'est pas valide
			{
				for(int k=0; k < N; k++)// 'k' designe l'indice du point P qui teste la valdité  
				{
					if(k != i && k != j)// P ne doit pas etre egale a A ou a B
					{
						if( (onLeft(T[i], T[j], T[k])) || (isIncluded(T[i], T[j], T[k])) ) // arc invalide pour l'enveloppe
						{
							ok = false;
						}
					}
				}
				if(ok)
				{
					listInsertLast(E, newDEdge(T[i], T[j]));
				}
			}
		}
	}
	List *H = DedgesToClockwisePoints(E); // organisation des arcs en liste de points dans le sens horaire de l'enveloppe convexe
	writeSolution(outfilename, H);

	// Libération de la mémoire
	for (int i = 0; i < N; i++)
	{
		freePoint(T[i]);
	}
	
	free(T);
	freeList(H,0);
}

/**
 * @brief Compare le points \p a et \p b.
 *
 * @param[in] a
 * @param[in] b
 * @return int si l'abscisse de \p a est plus petite que l'abscisse de \p b
 * renvoie 1, sinon renvoie 0. Dans le cas d'égalité, si l'ordonnée de \p a
 * est plus petite que l'ordonnée de \p b renvoie 1, sinon renvoie 0.
*/ 
static int smallerPoint(const void* a, const void* b)
{
	if(X((Point*)a) < X((Point*)b)){
		return 1;
	}
	else if(X((Point*)a) > X((Point*)b)){
		return 0;
	}
	else{
		if(Y((Point*)a) < Y((Point*)b)){
			return 1;
		}
		else{
			return 0;
		}
	}
}

/**
 * @brief Compare le points \p a et \p b.
 * 
 * @param[in] a 
 * @param[in] b 
 * @return int si l'abscisse de \p a est plus grande que l'abscisse de \p b
 * renvoie 1, sinon renvoie 0. Dans le cas d'égalité, si l'ordonnée de \p a
 * est plus grande que l'ordonnée de \p b renvoie 1, sinon renvoie 0.
 */
static int biggerPoint(const void* a, const void* b) 
{
	if(X((Point*)a) > X((Point*)b)){
		return 1;
	}
	else if(X((Point*)a) < X((Point*)b)){
		return 0;
	}
	else{
		if(Y((Point*)a) > Y((Point*)b)){
			return 1;
		}
		else{
			return 0;
		}
	}
}

void ConvexHull(const char* infilename, const char* outfilename, int sortby)
{ 
	int N;
	Point **P = readInstance(infilename,&N);
	switch (sortby)
	{
	case 1 :
		CBTHeapSort((void **)P,N,&smallerPoint,&viewPoint,&freePoint);
		break;
	case 2 :
		ArrayHeapSort((void **)P,N,&biggerPoint,&viewPoint,&freePoint);
		break;
	case 3 :
		SelectionSort((void **)P,N,&smallerPoint);
		break;	
	default:
		ShowMessage("algo.c ConvexHull : sortby invalide",1);
	}
	// Hsup <- P1, P2
	List *Hsup = newList(viewPoint,freePoint);
	listInsertFirst(Hsup,P[1]);
	listInsertFirst(Hsup,P[0]);

	// Création de l'enveloppe supérieur de l'enveloppe convexe 
	for (int i = 2; i < N; i++)
	{
		listInsertLast(Hsup,P[i]);
		while (getListSize(Hsup) > 2 && onLeft(getLNodeData(Predecessor(Predecessor(Tail(Hsup)))),getLNodeData(Predecessor(Tail(Hsup))),getLNodeData(Tail(Hsup))))
		{
			listRemoveNode(Hsup,Predecessor(Tail(Hsup)));
		} 
	}

	// Hinf <-  Pavant-dernier, Pdernier
	List *Hinf = newList(viewPoint,freePoint);
	listInsertFirst(Hinf,P[N-2]);
	listInsertFirst(Hinf,P[N-1]);

	// Création de l'enveloppe inférieur de l'enveloppe convexe
	for (int j = N-3; j >= 0; j--)
	{
		listInsertLast(Hinf,P[j]);
		while (getListSize(Hinf) > 2 && onLeft(getLNodeData(Predecessor(Predecessor(Tail(Hinf)))),getLNodeData(Predecessor(Tail(Hinf))),getLNodeData(Tail(Hinf))))	
		{
			listRemoveNode(Hinf,Predecessor(Tail(Hinf)));
		} 
	}
	// Suppression des dernier point de chaque liste car deja present en premier point de l'autre liste
	listRemoveLast(Hsup);
	listRemoveLast(Hinf);

	// Concatenation des listes pour créer l'enveloppe convexe
	List *H = listConcatenate(Hsup,Hinf);
	writeSolution(outfilename, H);

	// Libération de la mémoire
	for (int i = 0; i < N; i++)
	{
		freePoint(P[i]);
	}
	free(P);

	freeList(H,0);
}

void RapidConvexHull(const char* infilename, const char* outfilename)
{
	int N;
	Point **T = readInstance(infilename, &N);
	List *H = newList(viewPoint,freePoint);
	// Recherche du point le plus petit et insertion dans la liste solution 
	void *min = T[0];
	for(int i=1; i<N-1; i++){
		if(X(min) > X(T[i])){
			min = T[i];
		}
	}
	listInsertFirst(H, min);

	do	
	{
		// Insertion du premier point candidat
		int i=0;
		while (estDedans(H,T[i]))
		{
			i++;
		}
		listInsertLast(H,T[i]);
		
		for (int i = 0; i < N; i++)
		{
			void *A = getLNodeData(Predecessor(Tail(H)));// Pointeur sur l'avant-dernier point de la liste H
			void *B = getLNodeData(Tail(H));// Pointeur sur le dernier point de la liste H
			if ( T[i] != A && T[i] != B )
			{
				if(onLeft(A, B, T[i]) || isIncluded(A, B, T[i])) // vérification de la validité du point à T[i] par rapport au derniers point de la liste
				{
					listRemoveLast(H);
					listInsertLast(H, T[i]);
				}
			}
		}
	} while(getLNodeData(Head(H)) != getLNodeData(Tail(H))); // Enveloppe fini quand fermé donc quand la tete et la queue on le meme point 
	listRemoveLast(H); // Suppression du point en double
	writeSolution(outfilename,H);

	// Libération de la mémoire
	for (int i = 0; i < N; i++)
	{
		freePoint(T[i]);
	}
	free(T);

	freeList(H,0);
}
