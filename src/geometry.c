#include <stdio.h>
#include <stdlib.h>
#include "geometry.h"
#include "util.h"

Point* newPoint(long long int x, long long int y) 
{
	Point *P = (Point*)calloc(1,sizeof(Point)); // Allouement mémoire
	P->x=x;
	P->y=y;
	return P;
}

long long int X(const Point* P) 
{
	return P->x;
}

long long int Y(const Point* P) 
{
	return P->y;
}

void viewPoint(const void* P) 
{
	printf("(%lld,%lld)",X((Point*)P),Y((Point*)P));
}

void freePoint(void* P) 
{
	free(P);
	P=NULL;
}

int onRight(const Point* origin, const Point* destination, const Point* P) 
{
	return (((X(destination)-X(origin))*(Y(P)-Y(origin))-(Y(destination)-Y(origin))*(X(P)-X(origin))) < 0);
}

int onLeft(const Point* origin, const Point* destination, const Point* P) 
{
	return (((X(destination)-X(origin))*(Y(P)-Y(origin))-(Y(destination)-Y(origin))*(X(P)-X(origin))) > 0);
}

int collinear(const Point* origin, const Point* destination, const Point* P) 
{
	return (((X(destination)-X(origin))*(Y(P)-Y(origin))-(Y(destination)-Y(origin))*(X(P)-X(origin))) == 0);
}

int isIncluded(const Point* origin, const Point* destination, const Point* P) 
{ // P est inclu dans AB si P est collinaire à AB et avec des coordonnées entre celles de A et de B 
	return ((collinear(origin,destination,P)) && (((X(origin) <= X(P) && X(P) <= X(destination)) || ((X(origin) >= X(P) && X(P) >= X(destination)))) && ((Y(origin) <= Y(P) && Y(P) <= Y(destination)) || (Y(origin) >= Y(P) && Y(P) >= Y(destination)))));
}

DEdge* newDEdge(Point* origin, Point* destination) 
{
	DEdge *E=(DEdge*)calloc(1,sizeof(DEdge)); // Allouement mémoire
	E->destination=destination;
	E->origin=origin;
	return E;
}

Point* getOrigin(const DEdge* DE) 
{
	return DE->origin;
}

Point* getDestination(const DEdge* DE) 
{
	return DE->destination;
}

void viewDEdge(const void* DE) 
{
	viewPoint(getOrigin(DE));
	printf(" --> ");
	viewPoint(getDestination(DE));
}

void freeDEdge(void* DE) 
{
	free(DE);
	DE=NULL;
}