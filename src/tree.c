#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "tree.h"
#include "util.h"

/********************************************************************
 * TNode
 ********************************************************************/

TNode * newTNode(void* data) 
{
	TNode *NewNode=(TNode *)calloc(1,sizeof(TNode)); // Allouement mémoire
	NewNode->data = data;
	return NewNode;
}

void* getTNodeData(const TNode* node) 
{
	return node->data;
}

TNode* Left(const TNode* node) 
{
	return node->left;
}

TNode* Right(const TNode* node) 
{
	return node->right;
}

void setTNodeData(TNode* node, void* newData) 
{
	node->data = newData;
}

void setLeft(TNode* node, TNode* newLeft) 
{
	node->left = newLeft;
}

void setRight(TNode* node, TNode* newRight) 
{
	node->right = newRight;
}

/********************************************************************
 * Complete Binary Tree
 ********************************************************************/

CBTree * newCBTree(void (*viewData)(const void*), void (*freeData)(void*)) 
{
	CBTree *T=(CBTree*)calloc(1,sizeof(CBTree)); // Allouement mémoire
	T->viewData=(*viewData);
	T->freeData=(*freeData);
	resetCBTreeSize(T);
	return T;
}

int treeIsEmpty(CBTree* T) 
{
	return getCBTreeSize(T) == 0;
}

int getCBTreeSize(const CBTree* T) 
{
	return T->numelm;
}

TNode* Root(const CBTree* T) 
{
	return T->root;
}

void increaseCBTreeSize(CBTree* T) 
{
	T->numelm++;
}

void decreaseCBTreeSize(CBTree* T) 
{
	T->numelm--;
}

void resetCBTreeSize(CBTree* T) 
{
	T->numelm = 0;
}

void setRoot(CBTree* T, TNode* newRoot) 
{
	T->root = newRoot;
}

/**
 * @brief Libère récursivement le sous-arbre raciné au nœud \p node.
 * Dans le cas où le pointeur de fonction \p freeData n'est pas NULL,
 * la mémoire de la donnée du nœud actuel est aussi libérée.
 * NB : procédure récursive.
 * 
 * @param[in] node 
 * @param[in] freeData 
 */
static void freeTNode(TNode* node, void (*freeData)(void*)) 
{
	if (Left(node) != NULL) // Vérification que la node du fils gauche n'est pas déjà null
	{
		freeTNode(Left(node),freeData);
		if (Right(node) != NULL) // Vérification que la node du fils droit n'est pas déjà null
		{
			freeTNode(Right(node),freeData);
		}
	}
	if(freeData != NULL) // Vérification que freeData n'est pas déjà à null
	{
		(*freeData)(getTNodeData(node));
	}
	free(node);
	node = NULL;
}

/**
 * NB : Utilisez la procédure récursive freeTNode.
 * Vous devez initialiser le paramètre freeData
 * par rapport à la valeur de deleteData.
 */
void freeCBTree(CBTree * T, int deleteData) 
{
	if(!treeIsEmpty(T)) // On vérifie que l'arbre n'est pas déjà vide
	{
		if(deleteData == 1) // deleteData vaut 1 donc on libère la mémoire de la data en plus de la node
		{
			freeTNode(Root(T),T->freeData);
		}
		else // freeData sera initialisée à NULL, on libère seulement la node
		{
			freeTNode(Root(T),NULL);
		}
	}
	free(T);
	T=NULL;
}

/**
 * @brief Affiche les éléments de l'arbre raciné au nœud \p node
 * en réalisant un parcours préfixé.
 * Les données de chaque nœud sont afficher en utilisant le
 * pointer de fonction \p viewData.
 * 
 * @param[in] node 
 * @param[in] viewData 
 */
static void preorder(TNode *node, void (*viewData)(const void*)) 
{
	if(node != NULL) // Node doit exister sinon -> Exception
	{
		(viewData)(getTNodeData(node)); 
		printf(" ");
		preorder(Left(node),viewData); // Appel récursif sur fils gauche
		preorder(Right(node),viewData); // Appel récursif sur fils droit
	}
}

/**
 * @brief Affiche les éléments de l'arbre raciné au nœud \p node
 * en réalisant un parcours infixé.
 * Les données de chaque nœud sont afficher en utilisant le
 * pointer de fonction \p viewData.
 * 
 * @param[in] node 
 * @param[in] viewData 
 */
static void inorder(TNode *node, void (*viewData)(const void*)) 
{
	if(node != NULL) // Node doit exister sinon -> Exception
	{
		inorder(Left(node),viewData); // Appel récursif sur fils gauche
		(viewData)(getTNodeData(node));
		printf(" ");
		inorder(Right(node),viewData); // Appel récursif sur fils droit
	}
}

/**
 * @brief Affiche les éléments de l'arbre raciné au nœud \p node
 * en réalisant un parcours postfixé.
 * Les données de chaque nœud sont afficher en utilisant le
 * pointer de fonction \p viewData.
 * 
 * @param[in] node 
 * @param[in] viewData 
 */
static void postorder(TNode *node, void (*viewData)(const void*)) 
{
	if(node != NULL) // Node doit exister sinon -> Exception
	{
		postorder(Left(node),viewData); // Appel récursif sur fils gauche
		postorder(Right(node),viewData); // Appel récursif sur fils droit
		(viewData)(getTNodeData(node));
		printf(" ");
	}
}

/**
 * NB : Utilisez les procédures récursives preorder, inorder et postorder.
 * Rappel : order = 0 (preorder), 1 (postorder), 2 (inorder)
 */
void viewCBTree(const CBTree* T, int order) 
{
	switch(order) // Choix du parcours à afficher
	{
	case 0:
		printf("[ ");
		preorder(Root(T),T->viewData);
		printf("]");
		break;
	case 1:
		printf("[ ");
		postorder(Root(T),T->viewData);
		printf("]");
		break;
	case 2:
		printf("[ ");
		inorder(Root(T),T->viewData);
		printf("]");
		break;
	default:
		ShowMessage("erreur viewCBTree : order invalide",1);
		break;
	}
}

/**
 * @brief Insère récursivement un nouveau nœud de donnée \p data
 * dans l'arbre raciné au nœud \p node.
 * La position (par rapport à la racine \p node) où le nouveau nœud
 * va être insérer est indiquée par le paramètre \p position
 * (voir la figure ci-dessous pour la définition de la position).
 *  
 *          0
 *       /     \
 *      1       2
 *     / \     / \
 *    3   4   5   6
 *   / \
 *  7  ...
 * 
 * @param[in] node La racine de l'arbre actuel.
 * @param[in] position La position du nouveau élément
 * 						par rapport à la racine \p node.
 * @param[in] data La donnée à insérer.
 * @return TNode* Le nœud \p node mis à jour.
 */
static TNode* insertAfterLastTNode(TNode* node, int position, void* data) 
{
	if(position == 0) // Exception : position vaut 0 donc on est deja à la position recherchée
	{
		return newTNode(data);
	}
	else
	{
		int nb = position+1;
		int h = floor(log2(nb)+1); // Hauteur de l'arbre
		int k =  nb-(pow(2,(h-1))-1); 
		int t = pow(2,(h-1))/2;
		if(k <= t) // Position est dans le sous arbre à gauche de la node actuelle 
		{
			setLeft(node,insertAfterLastTNode(Left(node), (nb-pow(2, h-2)-1), data)); // Appel récursif sur le sous arbre gauche
		}
		else // Position est dans le sous arbre à droite de la node actuelle
		{
			setRight(node,insertAfterLastTNode(Right(node), (nb-pow(2, h-1)-1), data)); // Appel récursif sur le sous arbre droit
		}
	}
	return node;
}
/**
 * NB : Utilisez la procédure récursive insertAfterLastTNode
 * afin de lancer l'insertion.
 */
void CBTreeInsert(CBTree* T, void* data) 
{
	if(treeIsEmpty(T)) // Exception : arbre vide donc on initialise directement la data dans la racine
	{
		setRoot(T, newTNode(data));
	}
	else // On insère la valeur data à la fin de l'arbre
	{
		insertAfterLastTNode(Root(T), getCBTreeSize(T), data); 
	}
	increaseCBTreeSize(T);
}

/**
 * @brief Supprime récursivement le dernier nœud
 * de l'arbre raciné au nœud \p node.
 * La position (par rapport à la racine \p node) du nœud à supprimer
 * est indiquée par le paramètre \p position
 * (voir la figure ci-dessous pour la définition de la position).
 * La mémoire du dernier nœud est libérée mais pas la mémoire de sa donnée.
 *  
 *          0
 *       /     \
 *      1       2
 *     / \     / \
 *    3   4   5   6
 *   / \
 *  7  ...
 * 
 * @param[in] node La racine de l'arbre actuel.
 * @param[in] position La position de l'élément à supprimer
 *                         par rapport à la racine \p node.
 * @param[out] data La donnée du nœud supprimé (sortie).
 * @return TNode* Le nœud \p node mis à jour.
 */
static TNode* removeLastTNode(TNode* node, int position, void** data) 
{
	if(node != NULL)
	{
		if(position == 0) // Exception : position vaut 0 donc la position est déjà correcte, on peut libérer directement la node
		{
			*data = getTNodeData(node);
			free(node);
			node = NULL;
		}
		else if(position == 1) // Exception 2 : position vaut 1 donc la position est sur le fils gauche de la racine, on peut libérer directement la node à gauche
		{ 
			*data = getTNodeData(Left(node));
			free(Left(node));		
			setLeft(node,NULL);
		}
		else if(position == 2) // Exception 3 : position vaut 2 donc la position est sur le fils droit de la racine, on peut libérer directement la node à droite
		{
			*data = getTNodeData(Right(node)); 
			free(Right(node));
			setRight(node,NULL);
		}
		else 
		{
			int nb = position+1;
			int h = floor(log2(nb)+1); // Hauteur de l'arbre
			int k =  nb-(pow(2,(h-1))-1);
			int t = pow(2,(h-1))/2;
			if(k <= t) // Position est dans le sous arbre à gauche de la node actuelle 
			{
				removeLastTNode(Left(node),(nb-pow(2,h-2)-1),data); // Appel récursif sur le sous arbre gauche
			}
			else // Position est dans le sous arbre à droite de la node actuelle 
			{
				removeLastTNode(Right(node),(nb-pow(2,h-1)-1),data); // Appel récursif sur le sous arbre droit
			}
		}
	}
	return node;
}

/**
 * NB : Utilisez la procédure récursive removeLastTNode
 * afin de lancer la suppression.
 */
void* CBTreeRemove(CBTree* T) {
	assert(Root(T));
	void *data;
	removeLastTNode(Root(T),T->numelm-1,&data); // Suppression de l'arbre en partant de la racine
	decreaseCBTreeSize(T);
	return data;
}

/**
 * @brief Restitue récursivement le dernier nœud
 * de l'arbre raciné au nœud \p node.
 * La position (par rapport à la racine \p node) de ce dernier nœud
 * est indiquée par le paramètre \p position
 * (voir la figure ci-dessous pour la définition de la position).
 *  
 *          0
 *       /     \
 *      1       2
 *     / \     / \
 *    3   4   5   6
 *   / \
 *  7  ...
 * 
 * @param node La racine de l'arbre actuel.
 * @param position La position du dernier nœud par rapport à la racine \p node.
 * @return TNode* Le dernier nœud de l'arbre.
 */
static TNode* getLastTNode(TNode* node, int position) 
{
	TNode *res;
	if(node != NULL) 
	{
		if(position == 1) // Position vaut 1, on est sur le fils gauche de la racine
		{
			res = Left(node);
		}
		else if(position == 2) // Position vaut 2, on est sur le fils droit de la racine
		{
			res = Right(node);
		}
		else
		{
			int nb = position+1;
			int h = floor(log2(nb)+1); // Hauteur de l'arbre
			int k =  nb-( pow(2,(h-1))-1);
			int t = pow(2,(h-1))/2;
			if(k <= t) // Position est dans le sous arbre à gauche de la node actuelle 
			{
				res = getLastTNode(Left(node),(nb-pow(2,h-2)-1));
			}
			else // Position est dans le sous arbre à droite de la node actuelle 
			{
				res = getLastTNode(Right(node),(nb-pow(2,h-1)-1));
			}
		}
	}
	return res;
}

/**
 * NB : Utilisez la procédure récursive getLastTNode
 * afin de lancer la recherche.
 */
TNode* CBTreeGetLast(CBTree* T) 
{
	return getLastTNode(Root(T),T->numelm-1);
}

void CBTreeSwapData(TNode* node1, TNode* node2) 
{
	if (node1 == NULL || node2 == NULL) // Exception : les nodes ne peuvent être NULL
	{
		ShowMessage("CBTreeSwapData : node1 ou node2 == NULL",1);
	}
	// Echange des deux valeurs
	void *aux=getTNodeData(node1); 
	setTNodeData(node1,getTNodeData(node2));
	setTNodeData(node2,aux);
}