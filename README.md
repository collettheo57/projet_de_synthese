## Compilation du programme ##

    Pour compiler le programme, ouvrez le terminal dans le dossier contenant les sous-dossiers "src" et "include" (si dans ce dossier il n'y a pas de sous-dossier "obj" veuillez en créer un) puis entrez dans le terminal "make" ("make run" si vous vouler exécuter le programme en même temps)

## Format du fichier de points ##
    La première ligne doit contenir le nombre de points dans le fichier.
    Les lignes suivantes doivent contenir un seul point par ligne abscisse puis ordonné séparé par un espace.

    Exemple :

        4
        0 5
        8 6
        9 56
        78 3


## Utilisation du programme ##

    Deux manières d'utilisation vous sont possibles :
        - avant l'execution du programme dans le terminal, vous rajouter 3 arguments après "./convexhull", tous séparés par une espace, le premier argument est le fichier contenant les points dont vous souhaitez avoir l'envelope convexe, le deuxième argument est le fichier dans le quel la solution vous sera donné, et le troisième argument est un chiffre entre 1 et 5 indiquant la manière dont vous voulez calculer l'envelope convexe : 
            [1] pour SlowConvexHull
            [2] pour ConvexHull avec CBTHeapSort
            [3] pour ConvexHull avec ArrayHeapSort
            [4] pour ConvexHull avec SelectionSort
            [5] pour RapidConvexHull

        Exemple :

            ./convexhull fichier_de_points fichier_envelope_convexe 2


        - lors de l'execution dans le terminal, il vous sera demandé le fichier contenant les points dont vous souhaitez avoir l'envelope convexe, entrez le, puis il vous sera demandé le fichier dans lequel la solution vous sera donné. Enfin, il faudra entrer la manière dont vous voulez calculer l'envelope convexe, indicé de la même manière que pour une execution avec les arguments en appel.